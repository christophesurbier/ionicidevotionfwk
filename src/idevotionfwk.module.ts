import { NgModule, ModuleWithProviders } from '@angular/core';
import { iDevotionApiProvider } from './providers/idevotion-api-provider';
import { IonicModule } from '@ionic/angular';
import {iDevotionAppComponent} from './app/idevotion-app.component'; 
@NgModule({
    imports: [
        // Only if you use elements like ion-content, ion-xyz...
        IonicModule
    ],
    declarations: [
        // declare all components that your module uses
       // iDevotionLoginComponent
       iDevotionAppComponent
    ],
    exports: [
        // export the component(s) that you want others to be able to use
        //iDevotionLoginComponent
        iDevotionAppComponent
    ]
})
export class IDevotionFwkModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: IDevotionFwkModule,
            providers: [iDevotionApiProvider]
        };
    }
}