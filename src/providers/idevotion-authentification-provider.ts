import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { iDevotionApiProvider} from './idevotion-api-provider'

@Injectable({
  providedIn: 'root'
})
export class iDevotionAuthentificationProviderService {
 
  authenticationState = new BehaviorSubject(false);
 
  constructor(private storage: Storage, private plt: Platform,
    public apiService : iDevotionApiProvider) { 
     
  }
   
 
  isAuthenticated() {
    return this.authenticationState.value;
  }
 
}