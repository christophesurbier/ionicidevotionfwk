import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import CryptoJS from 'crypto-js';
import { Events } from '@ionic/angular';
import { AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable()
export class iDevotionApiProvider {

    virtualHostName: string = ''
    oAuth2ClientId: string = '';
    oAuth2ClientSecret: string = '';
    oAuth2Username: string = '';
    oAuth2Password: string = '';
    appName: string = '';
    tokenSSO: String = "";
    networkConnected: boolean = true;
    loader: any;
    expireDate: any;
    isOnline = false;
    // Standard url from the iDevotion Django API Fwk
    checkUrl: string = '';
    getFcmUrl: string = '';
    getOauthUrl: string = '';
    getAPnsUrl: string = '';
    sendMessageUrl: string = '';
    urlPwdOublie: string = '';
    uploadPhotoUrl: string = '';
    getActivateEmailUrl: string = '';
    getAppUserUrl: string = '';
    getAppUserAroundUrl: string = '';
    getImageFromUrl: string = '';

     /**
     * Initialize the provider with some variables specific to each application
     *
     *
     * @param url - The virtual host used to reach the API
     * @param app_name - The name of the application
     * @param clientId - The oAuth2 client id
     * @param clientSecret - The oAuth2 client secret
     * @param user - The oAuth2 username
     * @param password - The oAuth2 password
     * 
     *
     */
  
     initProvider(url, app_name, clientId, clientSecret, user, password) {
        this.virtualHostName = url;
        this.oAuth2ClientId = clientId;
        this.oAuth2ClientSecret = clientSecret;
        this.oAuth2Username = user;
        this.oAuth2Password = password;
        this.appName = app_name;
        this.initUrls()
    }

    private initUrls() {
        this.checkUrl = this.virtualHostName + "/checkAPI/"
        this.getFcmUrl = this.virtualHostName + "/devices/"
        this.getOauthUrl = this.virtualHostName + '/o/token/';
        this.getAPnsUrl = this.virtualHostName + "/device/apns/"
        this.sendMessageUrl = this.virtualHostName + "/sendMessageEmail"
        this.urlPwdOublie = this.virtualHostName + "/account/reset_password"
        this.getAppUserUrl = this.virtualHostName + "/appuser/"
        this.uploadPhotoUrl = this.virtualHostName + "/uploadUserPhotoBase64/"
        this.getActivateEmailUrl = this.virtualHostName + "/account/validate_account"
        this.getAppUserAroundUrl = this.virtualHostName + "/appuseraround/"
        this.getImageFromUrl = this.virtualHostName + "/getImageFromUrl/";

    }


    constructor(public http: HttpClient,
        public storage: Storage,
        public loadingController: LoadingController,
        public alertCtrl: AlertController,
        public events: Events) {

        console.log('--------- Initialisation ApiserviceProvider Provider');
        this.http = http
        this.storage = storage
    }


     /**
     * Show a loader inside your ionic application
     *
     */
    async showLoading() {
        console.log("SHOW LOADING")
        if (this.loader) {
            this.loader.dismiss();
            this.loader = null;
            console.log("déjà une popup en cours on l'arrete pour en démarrer une autre")
            // return
        }

        this.loader = await this.loadingController.create({
            message: 'Merci de patienter',
            duration: 4000
        });
        return await this.loader.present();

    }

     /**
     * Show a loader with a specific message inside your ionic application
     *
     */
    public async showLoadingMessage(message) {
        this.loader = await this.loadingController.create({
            message: message,
        });
        this.loader.present();
    }

     /**
     * Stop the loader inside your ionic application
     *
     */

    async stopLoading() {
        console.log("STOP LOADING?")
        this.loader.dismiss();

    }

     /**
     * Show default message when no network is available
     *
     */
    async showNoNetwork() {
        let alert = await this.alertCtrl.create({
            header: 'Désolé',
            message: 'Pas de réseau détecté. Merci de vérifier votre connexion 3G/4G ou Wifi',
            buttons: ['OK']
        });
        //Reverifie la connexion
        this.events.publish('checkNetwork');
        return await alert.present();

    }

     /**
     * Show error message  
     *
     * @param text - The message to show
     * 
     */
    async showError(text) {
        let alert = await this.alertCtrl.create({
            header: 'Erreur',
            message: text,
            buttons: ['OK']
        });
        return await alert.present();
    }

     /**
     * Show a message  
     *
     * @param title - The title of the message to show
     * @param message - The text of the message to show
     * 
     */
    async showMessage(title, message) {
        let alert = await this.alertCtrl.create({
            header: title,
            message: message,
            buttons: ['OK']
        });
        return await alert.present();
    }

    checkOauthToken() {

        return new Promise(resolve => {
            this.storage.get(this.appName + '_accessToken').then((result) => {
                if (result) {
                    this.tokenSSO = result
                    resolve(result)
                }
                else {
                    resolve()
                }
            });
        });
    }

    checkAPI() {
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.get(this.checkUrl)
                .pipe(
                    retry(1)
                )
                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                });
        });
    }

    getOAuthToken() {
        let url = this.getOauthUrl
        console.log("pas de token appel WS url avec nouveau headers " + url);
        let body = 'client_id=' + this.oAuth2ClientId + '&client_secret=' + this.oAuth2ClientSecret + '&username=' + this.oAuth2Username + '&password=' + this.oAuth2Password + '&grant_type=password';
        //console.log("body "+body);
        const httpOptions = {
            headers: new HttpHeaders({
                'content-type': "application/x-www-form-urlencoded",
            })
        };
        return new Promise(resolve => {
            this.http.post(url, body, httpOptions)
                .pipe(
                    retry(1)
                )
                .subscribe(res => {
                    let token = res["access_token"];
                    this.tokenSSO = token
                    console.log("ok TOKEN " + token);
                    this.storage.set(this.appName + '_accessToken',this.tokenSSO).then((result) => {
                        resolve(token)
                    })
                }, error => {
                    console.log("ERREUR APPEL TOKEN " + error);// Error getting the data
                    resolve()
                });
        });
    }

    sendMessageEmail(email, subject, message) {
        let urlParams = "?email=" + email + "&subject=" + subject + "&message=" + message;
        let url = this.sendMessageUrl + encodeURI(urlParams);
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'oAuth: ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        return new Promise(resolve => {
            this.http.get(url, httpOptions)
                .pipe(retry(1))
                .subscribe(res => {
                    resolve(res)
                }, error => {
                    console.log(error);// Error getting the data
                    resolve()
                });
        });
    }


    sendFcmToken(tokenId, deviceName, typeDevice) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        let postParams = {
            name: deviceName,
            registration_id: tokenId,
            active: true,
            type: typeDevice
        };
        console.log("on envoi TOKEN DEVICE " + JSON.stringify(postParams));
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.post(this.getFcmUrl, JSON.stringify(postParams), options)
                .pipe(retry(1))

                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }



    sendResetPasswordLink(email) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };

        let postParams = 'email_or_username=' + email;
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.post(this.urlPwdOublie, JSON.stringify(postParams), options)
                .pipe(retry(1))
                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }


    sendActivateLink(email) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };

        let postParams = 'email=' + email;
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.post(this.getActivateEmailUrl, JSON.stringify(postParams), options)
                .pipe(retry(1))
                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }



    createUser(userToCreate) {
        // user JSON
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };

        let params = JSON.stringify(userToCreate)
        console.log("json user qu'on envoi : " + params);
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.post(this.getAppUserUrl, userToCreate, options)
                .pipe(retry(1))
                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }

    findUserById(userId) {
        let body = '?id=' + userId;
        let url = this.getAppUserUrl + body;
        return this.findUser(url)
    }

    findUserByEmail(email) {
        let body = '?email=' + email;
        let url = this.getAppUserUrl + body;
        return this.findUser(url)
    }

    checkUserLoginAndPassword(email, password) {
        let body = '?email=' + email + "&password=" + password;
        let url = this.getAppUserUrl + body;
        return this.findUser(url)
    }



    checkUserByPseudoAndPassword(pseudo, password) {
        let passwordEncoded = CryptoJS.SHA256(password).toString(CryptoJS.enc.Hex);
        let body = '?pseudo=' + pseudo + "&password=" + passwordEncoded;
        let url = this.getAppUserUrl + body;
        return this.findUser(url)
    }

    findUserByPseudo(pseudo) {
        let body = '?pseudo=' + pseudo + "&valid=true";
        let url = this.getAppUserUrl + body;
        return this.findUser(url)
    }

    checkUserLoginAndPasswordToEncrypt(params) {
        let passwordEncoded = CryptoJS.SHA256(params.password).toString(CryptoJS.enc.Hex);
        return this.checkUserLoginAndPassword(params.email, passwordEncoded)
    }

    findUserByFacebookId(fbId, email) {
        let body = '?facebookId=' + fbId + "&email=" + email;
        let url = this.getAppUserUrl + body;
        return this.findUser(url)
    }

    findUser(url) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };

        console.log("on appelle BACKEND " + url);
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.get(url, options)
                .pipe(retry(1))
                .subscribe(res => {
                    observer.next(res);
                    observer.complete();
                }, error => {
                    observer.next();
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }


    patchUser(userId, patchParams) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };

        let json = JSON.stringify(patchParams)
        console.log("on patch contentType  url:" + this.getAppUserUrl + userId + "/" + " et json " + json)
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.patch(this.getAppUserUrl + userId + "/", patchParams, options)
                .pipe(retry(1))
                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }

    updateUser(userId, putParams) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.patch(this.getAppUserUrl + userId + "/", putParams, options)
                .pipe(retry(1))
                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }

    deleteUser(userId) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.delete(this.getAppUserUrl + userId + "/", options)
                .pipe(retry(1))
                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }

    findUserAround(distanceZoneRecherche, page, currentUserLatitude, currentUserLongitude) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        // Users : array of users Id
        let queryPath = this.getAppUserAroundUrl;
        let paramPath = ""

        queryPath += paramPath + "&page=" + page;

        // Calcule distance 
        let distance = 1000; // par défaut 1Km
        if (distanceZoneRecherche == 1) {
            //5Km
            distance = 5 * 1000;
        }
        else if (distanceZoneRecherche == 2) {
            //9Km
            distance = 9 * 1000;
        }
        else if (distanceZoneRecherche == 3) {
            //13
            distance = 13 * 1000;
        }
        else if (distanceZoneRecherche == 4) {
            distance = 17 * 1000;
        }
        else if (distanceZoneRecherche == 5) {
            // plus 
            distance = 1000000;
        }
        queryPath += "&dist=" + distance + "&point=" + currentUserLatitude + "," + currentUserLongitude;
        console.log("on appelle queryPath " + queryPath);
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.get(queryPath, options)
                .pipe(retry(1))
                .subscribe(res => {
                    observer.next(res);
                    observer.complete();
                }, error => {
                    observer.next();
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }



    uploadUserImage(userId, image) {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            }),
            responseType: 'text' as 'text'
        };

        let postParams = {
            "id": userId,
            "picture": image,
            "filename": newFileName,
        };
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.post(this.uploadPhotoUrl, postParams, options)
                .pipe(retry(1))
                .subscribe(res => {
                    this.networkConnected = true
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.next(false);
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }


    getImageFromURL(urlToGet: string) {
        const options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            }),
            responseType: 'text' as 'text'
        };
        let url = this.getImageFromUrl + "?url=" + encodeURI(urlToGet)
        console.log("Query  " + url)
        return Observable.create(observer => {
            // At this point make a request to your backend to make a real check!
            this.http.get(url, options)
                .pipe(retry(1))
                .subscribe(res => {
                    observer.next(res);
                    observer.complete();
                }, error => {
                    observer.next();
                    observer.complete();
                    console.log(error);// Error getting the data
                });
        });
    }
}