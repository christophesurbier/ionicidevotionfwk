import { Events } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { iDevotionApiProvider} from './idevotion-api-provider'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class iDevotionUserManagerProviderService {

  currentUser : any;

  constructor(public storage: Storage,public apiService : iDevotionApiProvider) {
    console.log('-------- INIT UserManagerProviderService Provider --------- ');
  }
 
  saveUser(){
    console.log("===== on sauve user "+JSON.stringify(this.currentUser));
    this.storage.set(this.apiService.appName+'_currentUser', JSON.stringify(this.currentUser));
  }

  setUser(user) {
    //let aUser = new User().initWithJSON(user);
    this.currentUser = user;
    this.storage.set(this.apiService.appName+'_currentUser', JSON.stringify(user));
  }

  getUser() {
    return new Promise(resolve => {
      console.log("on cherche cle "+this.apiService.appName+'_currentUser')
      this.storage.get(this.apiService.appName+'_currentUser').then((result) => {
        if(result){
          let user = JSON.parse(result);
          //console.log("on a trouve user pour cle "+JSON.stringify(user))
        //  let aUser = new User().initWithJSON(user);
          this.currentUser = user;
          resolve(this.currentUser)
        }
        else {
          resolve()
        }
      });
    });
  }


  uploadBase64UserImage(base64Image){
    return Observable.create(observer => {
      console.log("========== on envoi l'image à user "+this.currentUser.id);
      this.apiService.uploadUserImage(this.currentUser.id,base64Image).subscribe(res=> {
        observer.next(true);
        observer.complete();
      },error=>{
        console.log("erreur upload "+JSON.stringify(error))
        observer.next(false);
        observer.complete();
      });
    });
  }


  logoutUser() {
    return new Promise(resolve => {
      // on doit le passe offline
      console.log("-------- LOGOUT USER --------");
     
      let patchParams = {
        "online":false,
      }
      if (this.currentUser.id){
        this.apiService.patchUser(this.currentUser.id,patchParams).subscribe((resultat)=>{
        });
      }
      this.currentUser=null;
      this.storage.remove(this.apiService.appName+'_currentUser').then((result) => {
        resolve(true)
      });
    });
  }
}
