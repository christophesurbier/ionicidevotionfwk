import { Component,ViewChild } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {TranslateService} from '@ngx-translate/core';
import { iDevotionApiProvider} from '../providers/idevotion-api-provider';
import { Events } from '@ionic/angular';
import { ModalController,ActionSheetController,MenuController} from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { Badge } from '@ionic-native/badge/ngx';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import {iDevotionAuthentificationProviderService} from '../providers/idevotion-authentification-provider';
import {iDevotionUserManagerProviderService} from '../providers/idevotion-usermanager-provider';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class iDevotionAppComponent {
  userConnected = false;
  translate : TranslateService;
  networkConnected : boolean = true;
  tokenEnvoiEnCours : boolean = false;
  nbUnavailable : number = 0;
  page404Modal : any;
  task:any;

  private apiKey;
  private authDomain;
  private databaseURL;
  private projectId;
  private storageBucket;
  private messagingSenderId;

  initConfigFirebase(apiKey,authDomain,databaseURL,projectId,storageBucket,messagingSenderId){
    this.apiKey = apiKey;
    this.authDomain = authDomain;
    this.databaseURL = databaseURL;
    this.projectId = projectId;
    this.storageBucket = storageBucket;
    this.messagingSenderId = messagingSenderId;
  }

  private initFirebaseConfig(){
      if (this.apiKey){
        firebase.initializeApp({
            apiKey: this.apiKey,
            authDomain: this.authDomain,
            databaseURL: this.databaseURL,
            projectId: this.projectId,
            storageBucket: this.storageBucket,
            messagingSenderId: this.messagingSenderId
          });
      }
      else{
          console.log("=========== iDevotion FWK : Merci d'initialiser Firebase avant avec [initConfigFirebase]")
      }
   
  }

  translateConfig() {
    var userLang = navigator.language.split('-')[0]; // use navigator lang if available
    userLang = /(fr|en|de|es)/gi.test(userLang) ? userLang : 'en';
    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use(userLang);
  }

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    public actionSheetCtrl: ActionSheetController,
    public translateService: TranslateService,
    public network: Network,
    public apiService : iDevotionApiProvider,
    public userManager:iDevotionUserManagerProviderService,
    public events: Events,
    public storage : Storage,
    public badge: Badge,
    public modalCtrl: ModalController,
    public router : Router,
    public authentificationService:iDevotionAuthentificationProviderService) {

        console.log("----------- APP COMPONENT ---------");
        this.translate = translateService;
        this.translateConfig();
        this.initializeApp();
    }


    checkAPIAndMessage() {
      if (this.apiService.networkConnected){
        this.apiService.checkAPI().subscribe((success)=>{
          if (!success){
            this.nbUnavailable+=1;
            if (this.nbUnavailable>3){
              // Show page
              if (this.page404Modal==null){
                this.page404Modal = this.modalCtrl.create({
                  component: 'Page404ModalPage',
                  componentProps: {
                  }
                });
              }
            }
          }
          else {
            this.nbUnavailable = 0;
            if (this.page404Modal){
              this.page404Modal.dismiss();
              this.page404Modal=null;
            }
          }
        });
      }
      else {
        //Rechecke connection
        console.log("on check message : cnx KO");
        this.watchNetwork();
      }
    }

    watchNetwork() {
      console.log("================= WATCH NETWORK APP COMPONENT "+this.apiService.networkConnected+" ? ============")
      // watch network for a connection
      this.network.onConnect().subscribe(() => {
        console.log('=====>APP COMPONENT network connect :-(');
        this.apiService.networkConnected = true
        this.networkConnected = true;
        setTimeout(() => {
          if (this.network.type === 'wifi') {
            console.log('we got a wifi connection, woohoo!');
            this.apiService.networkConnected = true
            this.networkConnected = true;
          }
        }, 3000);
      });

      // watch network for a disconnect
      this.network.onDisconnect().subscribe(() => {
        console.log('=====>APP COMPONENT network was disconnected :-(');
        this.apiService.networkConnected = false;
        this.networkConnected = false;
      });
    }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      
      this.apiService.networkConnected = true;
      console.log("Platform READY INITIALISE FIREBASE");
      this.badge.clear();

       this.initFirebaseConfig() 

      this.watchNetwork();
      //Subscribe on resume
      this.platform.resume.subscribe(() => {
        if (this.userConnected){
          this.authentificationService.authenticationState.next(true)
          this.badge.clear();
          this.watchNetwork();
          this.changeStatus(true);
          
          this.task = setInterval(() => {
            this.checkAPIAndMessage();
          }, 10000);
        }
      });

      // Quit app
      this.platform.pause.subscribe(()=>{
        if (this.userConnected){
          this.goOffline();
          // End check Message and API
          if (this.task){
            clearInterval(this.task);
          }
        }
      });

      // event for login
      this.events.subscribe('login:done', () => {
        this.loginDone();
        this.userConnected = true
        this.authentificationService.authenticationState.next(true)
        this.changeStatus(true);
        
        this.task = setInterval(() => {
          this.checkAPIAndMessage();
        }, 10000);
      });

     
      
      // SplashScreen
      this.events.subscribe('splash:done', ()=>{
          this.splashDone();
      });

      // Path dans url ?
      if (document.URL.length>0){
        // let index = document.URL.indexOf('/') + 1
         let lastPath = window.location.pathname
         console.log("index PATH recu "+lastPath+" on va donc directement sur la page ?")
         if (lastPath.length>1){
           console.log("======= navigateByUrl /"+lastPath)
           this.loadTokenAndGoToPage(lastPath)
         }
         else{
           console.log("NE fait rien pour montrer SPLASH SCREEN")
          // this.loadTokenAndGoToPage("/accueil")
         }
       }
    
    });
  }

  loadTokenAndGoToPage(url){
    this.apiService.checkOauthToken().then((result) => {
      if (result){
        console.log("ACCESS DIRECT  Deja token "+result);
        this.splashDoneWithUrl("/"+url)
      }
      else {
        
        console.log("ACCESS DIRECT PAS DE TOKEN TOKEN")
        this.getToken()
      }
    });
  }


  getToken() {
    console.log("Pas deja token ")
    this.apiService.getOAuthToken().then((token) => {
      if (token){
        this.storage.set(this.apiService.appName+'_accessToken', token);
        console.log("Retour WS token "+token+" on sauve token en local "+token)
        if (this.platform.is("cordova")){
          this.splashScreen.hide();
        }
        this.authentificationService.authenticationState.next(false)
        this.authenticate();
      }
      else {
        console.log("AUCUN TOKEN RECUPERE !!!!!!!!!")
        this.apiService.showError(this.translate.instant("Impossible de communiquer avec nos serveurs. Merci de contacter 100Questions."));

      }
    });
  }

 

  loginDone(){
    //this.joueurManager.saveUser();
    console.log("--------- LOGIN DONE ---------")
    
  }

 
  splashDone(){
    this.splashDoneWithUrl("/accueil")
  }


  splashDoneWithUrl(goToUrl) {
    // si compte utilisateur existe déjà, on récupére data
    // on vérifie si toujours valide et si oui on log
    console.log("OK SPLASH DONE");
    this.userManager.getUser().then((userRetrieve)=>{
      if (userRetrieve){
        // récupére info joueur
        this.apiService.findUserById(this.userManager.currentUser.id).subscribe((user)=>{
          if (user){
            this.userManager.currentUser = user;
            this.userManager.saveUser();
            if (user.valide){
              this.authentificationService.authenticationState.next(true)
              this.userConnected = true;
             //this.initPushNotification();
              this.task = setInterval(() => {
                this.checkAPIAndMessage();
              }, 10000);

               //Check si id dans url 
               let index = goToUrl.lastIndexOf('/') + 1
               let endOfUrl = goToUrl.substr(index)
               console.log("endOfUrl est chiffre ? "+endOfUrl)
               if (Number(endOfUrl)){
                this.splashScreen.hide();
                       //Contexte perdu redirige sur accueil
                   this.router.navigateByUrl("/");
               }
               else {
                this.splashScreen.hide();
                   this.router.navigateByUrl(endOfUrl);
               }
                 
             
            }
            else {
              this.splashScreen.hide();
                this.authenticate();
              this.apiService.showError(this.translate.instant("Votre compte a été désactivé."));
            }
          }
          else {
            console.log("pas de user !!!")
            this.splashScreen.hide();
            this.authentificationService.authenticationState.next(false)
            this.authenticate();
            this.apiService.showError(this.translate.instant("Une erreur est survenue. Merci de revenir plus tard"));
          }
        });
      }
      else {
        this.splashScreen.hide();
        this.authentificationService.authenticationState.next(false)
        this.authenticate();
      }
    }).catch(error =>{
      console.log("error "+error);
    });
  }

  public authenticate() {
    //TO Override
  }
 
    changeStatus(online){
      console.log("ON CHANGE STATUS "+online);
      let patchParams = {
        "is_online":online,
        "lastConnexionDate": new Date(),
      }

      if (this.userManager.currentUser){
        this.apiService.patchUser(this.userManager.currentUser,patchParams).then((resultat)=>{
      });
    }
  }

  goOffline(){
    let patchParams = {
      "is_online":false,
    }

    if (this.userManager.currentUser){
      this.apiService.patchUser(this.userManager.currentUser,patchParams).then((resultat)=>{
      });
    }
  }
 
}
