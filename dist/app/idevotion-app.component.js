var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { iDevotionApiProvider } from '../providers/idevotion-api-provider';
import { Events } from '@ionic/angular';
import { ModalController, ActionSheetController, MenuController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { Badge } from '@ionic-native/badge/ngx';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { iDevotionAuthentificationProviderService } from '../providers/idevotion-authentification-provider';
import { iDevotionUserManagerProviderService } from '../providers/idevotion-usermanager-provider';
var iDevotionAppComponent = /** @class */ (function () {
    function iDevotionAppComponent(platform, statusBar, splashScreen, menuCtrl, actionSheetCtrl, translateService, network, apiService, userManager, events, storage, badge, modalCtrl, router, authentificationService) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.menuCtrl = menuCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.translateService = translateService;
        this.network = network;
        this.apiService = apiService;
        this.userManager = userManager;
        this.events = events;
        this.storage = storage;
        this.badge = badge;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.authentificationService = authentificationService;
        this.userConnected = false;
        this.networkConnected = true;
        this.tokenEnvoiEnCours = false;
        this.nbUnavailable = 0;
        console.log("----------- APP COMPONENT ---------");
        this.translate = translateService;
        this.translateConfig();
        this.initializeApp();
    }
    iDevotionAppComponent.prototype.initConfigFirebase = function (apiKey, authDomain, databaseURL, projectId, storageBucket, messagingSenderId) {
        this.apiKey = apiKey;
        this.authDomain = authDomain;
        this.databaseURL = databaseURL;
        this.projectId = projectId;
        this.storageBucket = storageBucket;
        this.messagingSenderId = messagingSenderId;
    };
    iDevotionAppComponent.prototype.initFirebaseConfig = function () {
        if (this.apiKey) {
            firebase.initializeApp({
                apiKey: this.apiKey,
                authDomain: this.authDomain,
                databaseURL: this.databaseURL,
                projectId: this.projectId,
                storageBucket: this.storageBucket,
                messagingSenderId: this.messagingSenderId
            });
        }
        else {
            console.log("=========== iDevotion FWK : Merci d'initialiser Firebase avant avec [initConfigFirebase]");
        }
    };
    iDevotionAppComponent.prototype.translateConfig = function () {
        var userLang = navigator.language.split('-')[0]; // use navigator lang if available
        userLang = /(fr|en|de|es)/gi.test(userLang) ? userLang : 'en';
        // this language will be used as a fallback when a translation isn't found in the current language
        this.translate.setDefaultLang('en');
        // the lang to use, if the lang isn't available, it will use the current loader to get them
        this.translate.use(userLang);
    };
    iDevotionAppComponent.prototype.checkAPIAndMessage = function () {
        var _this = this;
        if (this.apiService.networkConnected) {
            this.apiService.checkAPI().subscribe(function (success) {
                if (!success) {
                    _this.nbUnavailable += 1;
                    if (_this.nbUnavailable > 3) {
                        // Show page
                        if (_this.page404Modal == null) {
                            _this.page404Modal = _this.modalCtrl.create({
                                component: 'Page404ModalPage',
                                componentProps: {}
                            });
                        }
                    }
                }
                else {
                    _this.nbUnavailable = 0;
                    if (_this.page404Modal) {
                        _this.page404Modal.dismiss();
                        _this.page404Modal = null;
                    }
                }
            });
        }
        else {
            //Rechecke connection
            console.log("on check message : cnx KO");
            this.watchNetwork();
        }
    };
    iDevotionAppComponent.prototype.watchNetwork = function () {
        var _this = this;
        console.log("================= WATCH NETWORK APP COMPONENT " + this.apiService.networkConnected + " ? ============");
        // watch network for a connection
        this.network.onConnect().subscribe(function () {
            console.log('=====>APP COMPONENT network connect :-(');
            _this.apiService.networkConnected = true;
            _this.networkConnected = true;
            setTimeout(function () {
                if (_this.network.type === 'wifi') {
                    console.log('we got a wifi connection, woohoo!');
                    _this.apiService.networkConnected = true;
                    _this.networkConnected = true;
                }
            }, 3000);
        });
        // watch network for a disconnect
        this.network.onDisconnect().subscribe(function () {
            console.log('=====>APP COMPONENT network was disconnected :-(');
            _this.apiService.networkConnected = false;
            _this.networkConnected = false;
        });
    };
    iDevotionAppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.apiService.networkConnected = true;
            console.log("Platform READY INITIALISE FIREBASE");
            _this.badge.clear();
            _this.initFirebaseConfig();
            _this.watchNetwork();
            //Subscribe on resume
            _this.platform.resume.subscribe(function () {
                if (_this.userConnected) {
                    _this.authentificationService.authenticationState.next(true);
                    _this.badge.clear();
                    _this.watchNetwork();
                    _this.changeStatus(true);
                    _this.task = setInterval(function () {
                        _this.checkAPIAndMessage();
                    }, 10000);
                }
            });
            // Quit app
            _this.platform.pause.subscribe(function () {
                if (_this.userConnected) {
                    _this.goOffline();
                    // End check Message and API
                    if (_this.task) {
                        clearInterval(_this.task);
                    }
                }
            });
            // event for login
            _this.events.subscribe('login:done', function () {
                _this.loginDone();
                _this.userConnected = true;
                _this.authentificationService.authenticationState.next(true);
                _this.changeStatus(true);
                _this.task = setInterval(function () {
                    _this.checkAPIAndMessage();
                }, 10000);
            });
            // SplashScreen
            _this.events.subscribe('splash:done', function () {
                _this.splashDone();
            });
            // Path dans url ?
            if (document.URL.length > 0) {
                // let index = document.URL.indexOf('/') + 1
                var lastPath = window.location.pathname;
                console.log("index PATH recu " + lastPath + " on va donc directement sur la page ?");
                if (lastPath.length > 1) {
                    console.log("======= navigateByUrl /" + lastPath);
                    _this.loadTokenAndGoToPage(lastPath);
                }
                else {
                    console.log("NE fait rien pour montrer SPLASH SCREEN");
                    // this.loadTokenAndGoToPage("/accueil")
                }
            }
        });
    };
    iDevotionAppComponent.prototype.loadTokenAndGoToPage = function (url) {
        var _this = this;
        this.apiService.checkOauthToken().then(function (result) {
            if (result) {
                console.log("ACCESS DIRECT  Deja token " + result);
                _this.splashDoneWithUrl("/" + url);
            }
            else {
                console.log("ACCESS DIRECT PAS DE TOKEN TOKEN");
                _this.getToken();
            }
        });
    };
    iDevotionAppComponent.prototype.getToken = function () {
        var _this = this;
        console.log("Pas deja token ");
        this.apiService.getOAuthToken().then(function (token) {
            if (token) {
                _this.storage.set(_this.apiService.appName + '_accessToken', token);
                console.log("Retour WS token " + token + " on sauve token en local " + token);
                if (_this.platform.is("cordova")) {
                    _this.splashScreen.hide();
                }
                _this.authentificationService.authenticationState.next(false);
                _this.authenticate();
            }
            else {
                console.log("AUCUN TOKEN RECUPERE !!!!!!!!!");
                _this.apiService.showError(_this.translate.instant("Impossible de communiquer avec nos serveurs. Merci de contacter 100Questions."));
            }
        });
    };
    iDevotionAppComponent.prototype.loginDone = function () {
        //this.joueurManager.saveUser();
        console.log("--------- LOGIN DONE ---------");
    };
    iDevotionAppComponent.prototype.splashDone = function () {
        this.splashDoneWithUrl("/accueil");
    };
    iDevotionAppComponent.prototype.splashDoneWithUrl = function (goToUrl) {
        var _this = this;
        // si compte utilisateur existe déjà, on récupére data
        // on vérifie si toujours valide et si oui on log
        console.log("OK SPLASH DONE");
        this.userManager.getUser().then(function (userRetrieve) {
            if (userRetrieve) {
                // récupére info joueur
                _this.apiService.findUserById(_this.userManager.currentUser.id).subscribe(function (user) {
                    if (user) {
                        _this.userManager.currentUser = user;
                        _this.userManager.saveUser();
                        if (user.valide) {
                            _this.authentificationService.authenticationState.next(true);
                            _this.userConnected = true;
                            //this.initPushNotification();
                            _this.task = setInterval(function () {
                                _this.checkAPIAndMessage();
                            }, 10000);
                            //Check si id dans url 
                            var index = goToUrl.lastIndexOf('/') + 1;
                            var endOfUrl = goToUrl.substr(index);
                            console.log("endOfUrl est chiffre ? " + endOfUrl);
                            if (Number(endOfUrl)) {
                                _this.splashScreen.hide();
                                //Contexte perdu redirige sur accueil
                                _this.router.navigateByUrl("/");
                            }
                            else {
                                _this.splashScreen.hide();
                                _this.router.navigateByUrl(endOfUrl);
                            }
                        }
                        else {
                            _this.splashScreen.hide();
                            _this.authenticate();
                            _this.apiService.showError(_this.translate.instant("Votre compte a été désactivé."));
                        }
                    }
                    else {
                        console.log("pas de user !!!");
                        _this.splashScreen.hide();
                        _this.authentificationService.authenticationState.next(false);
                        _this.authenticate();
                        _this.apiService.showError(_this.translate.instant("Une erreur est survenue. Merci de revenir plus tard"));
                    }
                });
            }
            else {
                _this.splashScreen.hide();
                _this.authentificationService.authenticationState.next(false);
                _this.authenticate();
            }
        }).catch(function (error) {
            console.log("error " + error);
        });
    };
    iDevotionAppComponent.prototype.authenticate = function () {
        //TO Override
    };
    iDevotionAppComponent.prototype.changeStatus = function (online) {
        console.log("ON CHANGE STATUS " + online);
        var patchParams = {
            "is_online": online,
            "lastConnexionDate": new Date(),
        };
        if (this.userManager.currentUser) {
            this.apiService.patchUser(this.userManager.currentUser, patchParams).then(function (resultat) {
            });
        }
    };
    iDevotionAppComponent.prototype.goOffline = function () {
        var patchParams = {
            "is_online": false,
        };
        if (this.userManager.currentUser) {
            this.apiService.patchUser(this.userManager.currentUser, patchParams).then(function (resultat) {
            });
        }
    };
    iDevotionAppComponent = __decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html'
        }),
        __metadata("design:paramtypes", [Platform,
            StatusBar,
            SplashScreen,
            MenuController,
            ActionSheetController,
            TranslateService,
            Network,
            iDevotionApiProvider,
            iDevotionUserManagerProviderService,
            Events,
            Storage,
            Badge,
            ModalController,
            Router,
            iDevotionAuthentificationProviderService])
    ], iDevotionAppComponent);
    return iDevotionAppComponent;
}());
export { iDevotionAppComponent };
//# sourceMappingURL=idevotion-app.component.js.map