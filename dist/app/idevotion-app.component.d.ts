import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { iDevotionApiProvider } from '../providers/idevotion-api-provider';
import { Events } from '@ionic/angular';
import { ModalController, ActionSheetController, MenuController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { Badge } from '@ionic-native/badge/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { iDevotionAuthentificationProviderService } from '../providers/idevotion-authentification-provider';
import { iDevotionUserManagerProviderService } from '../providers/idevotion-usermanager-provider';
export declare class iDevotionAppComponent {
    platform: Platform;
    statusBar: StatusBar;
    splashScreen: SplashScreen;
    menuCtrl: MenuController;
    actionSheetCtrl: ActionSheetController;
    translateService: TranslateService;
    network: Network;
    apiService: iDevotionApiProvider;
    userManager: iDevotionUserManagerProviderService;
    events: Events;
    storage: Storage;
    badge: Badge;
    modalCtrl: ModalController;
    router: Router;
    authentificationService: iDevotionAuthentificationProviderService;
    userConnected: boolean;
    translate: TranslateService;
    networkConnected: boolean;
    tokenEnvoiEnCours: boolean;
    nbUnavailable: number;
    page404Modal: any;
    task: any;
    private apiKey;
    private authDomain;
    private databaseURL;
    private projectId;
    private storageBucket;
    private messagingSenderId;
    initConfigFirebase(apiKey: any, authDomain: any, databaseURL: any, projectId: any, storageBucket: any, messagingSenderId: any): void;
    private initFirebaseConfig;
    translateConfig(): void;
    constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, menuCtrl: MenuController, actionSheetCtrl: ActionSheetController, translateService: TranslateService, network: Network, apiService: iDevotionApiProvider, userManager: iDevotionUserManagerProviderService, events: Events, storage: Storage, badge: Badge, modalCtrl: ModalController, router: Router, authentificationService: iDevotionAuthentificationProviderService);
    checkAPIAndMessage(): void;
    watchNetwork(): void;
    initializeApp(): void;
    loadTokenAndGoToPage(url: any): void;
    getToken(): void;
    loginDone(): void;
    splashDone(): void;
    splashDoneWithUrl(goToUrl: any): void;
    authenticate(): void;
    changeStatus(online: any): void;
    goOffline(): void;
}
