import { ModuleWithProviders } from '@angular/core';
export declare class IDevotionFwkModule {
    static forRoot(): ModuleWithProviders;
}
