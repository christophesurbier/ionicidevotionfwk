var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { iDevotionApiProvider } from './providers/idevotion-api-provider';
import { IonicModule } from '@ionic/angular';
import { iDevotionAppComponent } from './app/idevotion-app.component';
var IDevotionFwkModule = /** @class */ (function () {
    function IDevotionFwkModule() {
    }
    IDevotionFwkModule_1 = IDevotionFwkModule;
    IDevotionFwkModule.forRoot = function () {
        return {
            ngModule: IDevotionFwkModule_1,
            providers: [iDevotionApiProvider]
        };
    };
    var IDevotionFwkModule_1;
    IDevotionFwkModule = IDevotionFwkModule_1 = __decorate([
        NgModule({
            imports: [
                // Only if you use elements like ion-content, ion-xyz...
                IonicModule
            ],
            declarations: [
                // declare all components that your module uses
                // iDevotionLoginComponent
                iDevotionAppComponent
            ],
            exports: [
                // export the component(s) that you want others to be able to use
                //iDevotionLoginComponent
                iDevotionAppComponent
            ]
        })
    ], IDevotionFwkModule);
    return IDevotionFwkModule;
}());
export { IDevotionFwkModule };
//# sourceMappingURL=idevotionfwk.module.js.map