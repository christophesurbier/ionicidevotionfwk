import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { iDevotionApiProvider } from './idevotion-api-provider';
export declare class iDevotionAuthentificationProviderService {
    private storage;
    private plt;
    apiService: iDevotionApiProvider;
    authenticationState: BehaviorSubject<boolean>;
    constructor(storage: Storage, plt: Platform, apiService: iDevotionApiProvider);
    isAuthenticated(): boolean;
}
