var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { iDevotionApiProvider } from './idevotion-api-provider';
import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@ionic/storage";
import * as i2 from "./idevotion-api-provider";
var iDevotionUserManagerProviderService = /** @class */ (function () {
    function iDevotionUserManagerProviderService(storage, apiService) {
        this.storage = storage;
        this.apiService = apiService;
        console.log('-------- INIT UserManagerProviderService Provider --------- ');
    }
    iDevotionUserManagerProviderService.prototype.saveUser = function () {
        console.log("===== on sauve user " + JSON.stringify(this.currentUser));
        this.storage.set(this.apiService.appName + '_currentUser', JSON.stringify(this.currentUser));
    };
    iDevotionUserManagerProviderService.prototype.setUser = function (user) {
        //let aUser = new User().initWithJSON(user);
        this.currentUser = user;
        this.storage.set(this.apiService.appName + '_currentUser', JSON.stringify(user));
    };
    iDevotionUserManagerProviderService.prototype.getUser = function () {
        var _this = this;
        return new Promise(function (resolve) {
            console.log("on cherche cle " + _this.apiService.appName + '_currentUser');
            _this.storage.get(_this.apiService.appName + '_currentUser').then(function (result) {
                if (result) {
                    var user = JSON.parse(result);
                    //console.log("on a trouve user pour cle "+JSON.stringify(user))
                    //  let aUser = new User().initWithJSON(user);
                    _this.currentUser = user;
                    resolve(_this.currentUser);
                }
                else {
                    resolve();
                }
            });
        });
    };
    iDevotionUserManagerProviderService.prototype.uploadBase64UserImage = function (base64Image) {
        var _this = this;
        return Observable.create(function (observer) {
            console.log("========== on envoi l'image à user " + _this.currentUser.id);
            _this.apiService.uploadUserImage(_this.currentUser.id, base64Image).subscribe(function (res) {
                observer.next(true);
                observer.complete();
            }, function (error) {
                console.log("erreur upload " + JSON.stringify(error));
                observer.next(false);
                observer.complete();
            });
        });
    };
    iDevotionUserManagerProviderService.prototype.logoutUser = function () {
        var _this = this;
        return new Promise(function (resolve) {
            // on doit le passe offline
            console.log("-------- LOGOUT USER --------");
            var patchParams = {
                "online": false,
            };
            if (_this.currentUser.id) {
                _this.apiService.patchUser(_this.currentUser.id, patchParams).subscribe(function (resultat) {
                });
            }
            _this.currentUser = null;
            _this.storage.remove(_this.apiService.appName + '_currentUser').then(function (result) {
                resolve(true);
            });
        });
    };
    iDevotionUserManagerProviderService.ngInjectableDef = i0.defineInjectable({ factory: function iDevotionUserManagerProviderService_Factory() { return new iDevotionUserManagerProviderService(i0.inject(i1.Storage), i0.inject(i2.iDevotionApiProvider)); }, token: iDevotionUserManagerProviderService, providedIn: "root" });
    iDevotionUserManagerProviderService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [Storage, iDevotionApiProvider])
    ], iDevotionUserManagerProviderService);
    return iDevotionUserManagerProviderService;
}());
export { iDevotionUserManagerProviderService };
//# sourceMappingURL=idevotion-usermanager-provider.js.map