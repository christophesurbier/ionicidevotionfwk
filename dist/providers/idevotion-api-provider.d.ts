import { HttpClient } from '@angular/common/http';
import { Events } from '@ionic/angular';
import { AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
export declare class iDevotionApiProvider {
    http: HttpClient;
    storage: Storage;
    loadingController: LoadingController;
    alertCtrl: AlertController;
    events: Events;
    virtualHostName: string;
    oAuth2ClientId: string;
    oAuth2ClientSecret: string;
    oAuth2Username: string;
    oAuth2Password: string;
    appName: string;
    tokenSSO: String;
    networkConnected: boolean;
    loader: any;
    expireDate: any;
    isOnline: boolean;
    checkUrl: string;
    getFcmUrl: string;
    getOauthUrl: string;
    getAPnsUrl: string;
    sendMessageUrl: string;
    urlPwdOublie: string;
    uploadPhotoUrl: string;
    getActivateEmailUrl: string;
    getAppUserUrl: string;
    getAppUserAroundUrl: string;
    getImageFromUrl: string;
    /**
    * Initialize the provider with some variables specific to each application
    *
    *
    * @param url - The virtual host used to reach the API
    * @param app_name - The name of the application
    * @param clientId - The oAuth2 client id
    * @param clientSecret - The oAuth2 client secret
    * @param user - The oAuth2 username
    * @param password - The oAuth2 password
    *
    *
    */
    initProvider(url: any, app_name: any, clientId: any, clientSecret: any, user: any, password: any): void;
    private initUrls;
    constructor(http: HttpClient, storage: Storage, loadingController: LoadingController, alertCtrl: AlertController, events: Events);
    /**
    * Show a loader inside your ionic application
    *
    */
    showLoading(): Promise<any>;
    /**
    * Show a loader with a specific message inside your ionic application
    *
    */
    showLoadingMessage(message: any): Promise<void>;
    /**
    * Stop the loader inside your ionic application
    *
    */
    stopLoading(): Promise<void>;
    /**
    * Show default message when no network is available
    *
    */
    showNoNetwork(): Promise<void>;
    /**
    * Show error message
    *
    * @param text - The message to show
    *
    */
    showError(text: any): Promise<void>;
    /**
    * Show a message
    *
    * @param title - The title of the message to show
    * @param message - The text of the message to show
    *
    */
    showMessage(title: any, message: any): Promise<void>;
    checkOauthToken(): Promise<{}>;
    checkAPI(): any;
    getOAuthToken(): Promise<{}>;
    sendMessageEmail(email: any, subject: any, message: any): Promise<{}>;
    sendFcmToken(tokenId: any, deviceName: any, typeDevice: any): any;
    sendResetPasswordLink(email: any): any;
    sendActivateLink(email: any): any;
    createUser(userToCreate: any): any;
    findUserById(userId: any): any;
    findUserByEmail(email: any): any;
    checkUserLoginAndPassword(email: any, password: any): any;
    checkUserByPseudoAndPassword(pseudo: any, password: any): any;
    findUserByPseudo(pseudo: any): any;
    checkUserLoginAndPasswordToEncrypt(params: any): any;
    findUserByFacebookId(fbId: any, email: any): any;
    findUser(url: any): any;
    patchUser(userId: any, patchParams: any): any;
    updateUser(userId: any, putParams: any): any;
    deleteUser(userId: any): any;
    findUserAround(distanceZoneRecherche: any, page: any, currentUserLatitude: any, currentUserLongitude: any): any;
    uploadUserImage(userId: any, image: any): any;
    getImageFromURL(urlToGet: string): any;
}
