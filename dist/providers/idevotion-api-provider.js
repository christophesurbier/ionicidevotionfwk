var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import CryptoJS from 'crypto-js';
import { Events } from '@ionic/angular';
import { AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
var iDevotionApiProvider = /** @class */ (function () {
    function iDevotionApiProvider(http, storage, loadingController, alertCtrl, events) {
        this.http = http;
        this.storage = storage;
        this.loadingController = loadingController;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.virtualHostName = '';
        this.oAuth2ClientId = '';
        this.oAuth2ClientSecret = '';
        this.oAuth2Username = '';
        this.oAuth2Password = '';
        this.appName = '';
        this.tokenSSO = "";
        this.networkConnected = true;
        this.isOnline = false;
        // Standard url from the iDevotion Django API Fwk
        this.checkUrl = '';
        this.getFcmUrl = '';
        this.getOauthUrl = '';
        this.getAPnsUrl = '';
        this.sendMessageUrl = '';
        this.urlPwdOublie = '';
        this.uploadPhotoUrl = '';
        this.getActivateEmailUrl = '';
        this.getAppUserUrl = '';
        this.getAppUserAroundUrl = '';
        this.getImageFromUrl = '';
        console.log('--------- Initialisation ApiserviceProvider Provider');
        this.http = http;
        this.storage = storage;
    }
    /**
    * Initialize the provider with some variables specific to each application
    *
    *
    * @param url - The virtual host used to reach the API
    * @param app_name - The name of the application
    * @param clientId - The oAuth2 client id
    * @param clientSecret - The oAuth2 client secret
    * @param user - The oAuth2 username
    * @param password - The oAuth2 password
    *
    *
    */
    iDevotionApiProvider.prototype.initProvider = function (url, app_name, clientId, clientSecret, user, password) {
        this.virtualHostName = url;
        this.oAuth2ClientId = clientId;
        this.oAuth2ClientSecret = clientSecret;
        this.oAuth2Username = user;
        this.oAuth2Password = password;
        this.appName = app_name;
        this.initUrls();
    };
    iDevotionApiProvider.prototype.initUrls = function () {
        this.checkUrl = this.virtualHostName + "/checkAPI/";
        this.getFcmUrl = this.virtualHostName + "/devices/";
        this.getOauthUrl = this.virtualHostName + '/o/token/';
        this.getAPnsUrl = this.virtualHostName + "/device/apns/";
        this.sendMessageUrl = this.virtualHostName + "/sendMessageEmail";
        this.urlPwdOublie = this.virtualHostName + "/account/reset_password";
        this.getAppUserUrl = this.virtualHostName + "/appuser/";
        this.uploadPhotoUrl = this.virtualHostName + "/uploadUserPhotoBase64/";
        this.getActivateEmailUrl = this.virtualHostName + "/account/validate_account";
        this.getAppUserAroundUrl = this.virtualHostName + "/appuseraround/";
        this.getImageFromUrl = this.virtualHostName + "/getImageFromUrl/";
    };
    /**
    * Show a loader inside your ionic application
    *
    */
    iDevotionApiProvider.prototype.showLoading = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        console.log("SHOW LOADING");
                        if (this.loader) {
                            this.loader.dismiss();
                            this.loader = null;
                            console.log("déjà une popup en cours on l'arrete pour en démarrer une autre");
                            // return
                        }
                        _a = this;
                        return [4 /*yield*/, this.loadingController.create({
                                message: 'Merci de patienter',
                                duration: 4000
                            })];
                    case 1:
                        _a.loader = _b.sent();
                        return [4 /*yield*/, this.loader.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    /**
    * Show a loader with a specific message inside your ionic application
    *
    */
    iDevotionApiProvider.prototype.showLoadingMessage = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingController.create({
                                message: message,
                            })];
                    case 1:
                        _a.loader = _b.sent();
                        this.loader.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
    * Stop the loader inside your ionic application
    *
    */
    iDevotionApiProvider.prototype.stopLoading = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("STOP LOADING?");
                this.loader.dismiss();
                return [2 /*return*/];
            });
        });
    };
    /**
    * Show default message when no network is available
    *
    */
    iDevotionApiProvider.prototype.showNoNetwork = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Désolé',
                            message: 'Pas de réseau détecté. Merci de vérifier votre connexion 3G/4G ou Wifi',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        //Reverifie la connexion
                        this.events.publish('checkNetwork');
                        return [4 /*yield*/, alert.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
    * Show error message
    *
    * @param text - The message to show
    *
    */
    iDevotionApiProvider.prototype.showError = function (text) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Erreur',
                            message: text,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
    * Show a message
    *
    * @param title - The title of the message to show
    * @param message - The text of the message to show
    *
    */
    iDevotionApiProvider.prototype.showMessage = function (title, message) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: title,
                            message: message,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    iDevotionApiProvider.prototype.checkOauthToken = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.storage.get(_this.appName + '_accessToken').then(function (result) {
                if (result) {
                    _this.tokenSSO = result;
                    resolve(result);
                }
                else {
                    resolve();
                }
            });
        });
    };
    iDevotionApiProvider.prototype.checkAPI = function () {
        var _this = this;
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.get(_this.checkUrl)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
            });
        });
    };
    iDevotionApiProvider.prototype.getOAuthToken = function () {
        var _this = this;
        var url = this.getOauthUrl;
        console.log("pas de token appel WS url avec nouveau headers " + url);
        var body = 'client_id=' + this.oAuth2ClientId + '&client_secret=' + this.oAuth2ClientSecret + '&username=' + this.oAuth2Username + '&password=' + this.oAuth2Password + '&grant_type=password';
        //console.log("body "+body);
        var httpOptions = {
            headers: new HttpHeaders({
                'content-type': "application/x-www-form-urlencoded",
            })
        };
        return new Promise(function (resolve) {
            _this.http.post(url, body, httpOptions)
                .pipe(retry(1))
                .subscribe(function (res) {
                var token = res["access_token"];
                _this.tokenSSO = token;
                console.log("ok TOKEN " + token);
                _this.storage.set(_this.appName + '_accessToken', _this.tokenSSO).then(function (result) {
                    resolve(token);
                });
            }, function (error) {
                console.log("ERREUR APPEL TOKEN " + error); // Error getting the data
                resolve();
            });
        });
    };
    iDevotionApiProvider.prototype.sendMessageEmail = function (email, subject, message) {
        var _this = this;
        var urlParams = "?email=" + email + "&subject=" + subject + "&message=" + message;
        var url = this.sendMessageUrl + encodeURI(urlParams);
        var httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'oAuth: ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        return new Promise(function (resolve) {
            _this.http.get(url, httpOptions)
                .pipe(retry(1))
                .subscribe(function (res) {
                resolve(res);
            }, function (error) {
                console.log(error); // Error getting the data
                resolve();
            });
        });
    };
    iDevotionApiProvider.prototype.sendFcmToken = function (tokenId, deviceName, typeDevice) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        var postParams = {
            name: deviceName,
            registration_id: tokenId,
            active: true,
            type: typeDevice
        };
        console.log("on envoi TOKEN DEVICE " + JSON.stringify(postParams));
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.post(_this.getFcmUrl, JSON.stringify(postParams), options)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.sendResetPasswordLink = function (email) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        var postParams = 'email_or_username=' + email;
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.post(_this.urlPwdOublie, JSON.stringify(postParams), options)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.sendActivateLink = function (email) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        var postParams = 'email=' + email;
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.post(_this.getActivateEmailUrl, JSON.stringify(postParams), options)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.createUser = function (userToCreate) {
        var _this = this;
        // user JSON
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        var params = JSON.stringify(userToCreate);
        console.log("json user qu'on envoi : " + params);
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.post(_this.getAppUserUrl, userToCreate, options)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.findUserById = function (userId) {
        var body = '?id=' + userId;
        var url = this.getAppUserUrl + body;
        return this.findUser(url);
    };
    iDevotionApiProvider.prototype.findUserByEmail = function (email) {
        var body = '?email=' + email;
        var url = this.getAppUserUrl + body;
        return this.findUser(url);
    };
    iDevotionApiProvider.prototype.checkUserLoginAndPassword = function (email, password) {
        var body = '?email=' + email + "&password=" + password;
        var url = this.getAppUserUrl + body;
        return this.findUser(url);
    };
    iDevotionApiProvider.prototype.checkUserByPseudoAndPassword = function (pseudo, password) {
        var passwordEncoded = CryptoJS.SHA256(password).toString(CryptoJS.enc.Hex);
        var body = '?pseudo=' + pseudo + "&password=" + passwordEncoded;
        var url = this.getAppUserUrl + body;
        return this.findUser(url);
    };
    iDevotionApiProvider.prototype.findUserByPseudo = function (pseudo) {
        var body = '?pseudo=' + pseudo + "&valid=true";
        var url = this.getAppUserUrl + body;
        return this.findUser(url);
    };
    iDevotionApiProvider.prototype.checkUserLoginAndPasswordToEncrypt = function (params) {
        var passwordEncoded = CryptoJS.SHA256(params.password).toString(CryptoJS.enc.Hex);
        return this.checkUserLoginAndPassword(params.email, passwordEncoded);
    };
    iDevotionApiProvider.prototype.findUserByFacebookId = function (fbId, email) {
        var body = '?facebookId=' + fbId + "&email=" + email;
        var url = this.getAppUserUrl + body;
        return this.findUser(url);
    };
    iDevotionApiProvider.prototype.findUser = function (url) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        console.log("on appelle BACKEND " + url);
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.get(url, options)
                .pipe(retry(1))
                .subscribe(function (res) {
                observer.next(res);
                observer.complete();
            }, function (error) {
                observer.next();
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.patchUser = function (userId, patchParams) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        var json = JSON.stringify(patchParams);
        console.log("on patch contentType  url:" + this.getAppUserUrl + userId + "/" + " et json " + json);
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.patch(_this.getAppUserUrl + userId + "/", patchParams, options)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.updateUser = function (userId, putParams) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.patch(_this.getAppUserUrl + userId + "/", putParams, options)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.deleteUser = function (userId) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.delete(_this.getAppUserUrl + userId + "/", options)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.findUserAround = function (distanceZoneRecherche, page, currentUserLatitude, currentUserLongitude) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            })
        };
        // Users : array of users Id
        var queryPath = this.getAppUserAroundUrl;
        var paramPath = "";
        queryPath += paramPath + "&page=" + page;
        // Calcule distance 
        var distance = 1000; // par défaut 1Km
        if (distanceZoneRecherche == 1) {
            //5Km
            distance = 5 * 1000;
        }
        else if (distanceZoneRecherche == 2) {
            //9Km
            distance = 9 * 1000;
        }
        else if (distanceZoneRecherche == 3) {
            //13
            distance = 13 * 1000;
        }
        else if (distanceZoneRecherche == 4) {
            distance = 17 * 1000;
        }
        else if (distanceZoneRecherche == 5) {
            // plus 
            distance = 1000000;
        }
        queryPath += "&dist=" + distance + "&point=" + currentUserLatitude + "," + currentUserLongitude;
        console.log("on appelle queryPath " + queryPath);
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.get(queryPath, options)
                .pipe(retry(1))
                .subscribe(function (res) {
                observer.next(res);
                observer.complete();
            }, function (error) {
                observer.next();
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.uploadUserImage = function (userId, image) {
        var _this = this;
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            }),
            responseType: 'text'
        };
        var postParams = {
            "id": userId,
            "picture": image,
            "filename": newFileName,
        };
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.post(_this.uploadPhotoUrl, postParams, options)
                .pipe(retry(1))
                .subscribe(function (res) {
                _this.networkConnected = true;
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider.prototype.getImageFromURL = function (urlToGet) {
        var _this = this;
        var options = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.tokenSSO,
                'Content-Type': 'application/json'
            }),
            responseType: 'text'
        };
        var url = this.getImageFromUrl + "?url=" + encodeURI(urlToGet);
        console.log("Query  " + url);
        return Observable.create(function (observer) {
            // At this point make a request to your backend to make a real check!
            _this.http.get(url, options)
                .pipe(retry(1))
                .subscribe(function (res) {
                observer.next(res);
                observer.complete();
            }, function (error) {
                observer.next();
                observer.complete();
                console.log(error); // Error getting the data
            });
        });
    };
    iDevotionApiProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient,
            Storage,
            LoadingController,
            AlertController,
            Events])
    ], iDevotionApiProvider);
    return iDevotionApiProvider;
}());
export { iDevotionApiProvider };
//# sourceMappingURL=idevotion-api-provider.js.map