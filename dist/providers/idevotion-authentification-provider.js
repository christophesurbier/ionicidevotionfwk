var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { iDevotionApiProvider } from './idevotion-api-provider';
import * as i0 from "@angular/core";
import * as i1 from "@ionic/storage";
import * as i2 from "@ionic/angular";
import * as i3 from "./idevotion-api-provider";
var iDevotionAuthentificationProviderService = /** @class */ (function () {
    function iDevotionAuthentificationProviderService(storage, plt, apiService) {
        this.storage = storage;
        this.plt = plt;
        this.apiService = apiService;
        this.authenticationState = new BehaviorSubject(false);
    }
    iDevotionAuthentificationProviderService.prototype.isAuthenticated = function () {
        return this.authenticationState.value;
    };
    iDevotionAuthentificationProviderService.ngInjectableDef = i0.defineInjectable({ factory: function iDevotionAuthentificationProviderService_Factory() { return new iDevotionAuthentificationProviderService(i0.inject(i1.Storage), i0.inject(i2.Platform), i0.inject(i3.iDevotionApiProvider)); }, token: iDevotionAuthentificationProviderService, providedIn: "root" });
    iDevotionAuthentificationProviderService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [Storage, Platform,
            iDevotionApiProvider])
    ], iDevotionAuthentificationProviderService);
    return iDevotionAuthentificationProviderService;
}());
export { iDevotionAuthentificationProviderService };
//# sourceMappingURL=idevotion-authentification-provider.js.map