import { Storage } from '@ionic/storage';
import { iDevotionApiProvider } from './idevotion-api-provider';
export declare class iDevotionUserManagerProviderService {
    storage: Storage;
    apiService: iDevotionApiProvider;
    currentUser: any;
    constructor(storage: Storage, apiService: iDevotionApiProvider);
    saveUser(): void;
    setUser(user: any): void;
    getUser(): Promise<{}>;
    uploadBase64UserImage(base64Image: any): any;
    logoutUser(): Promise<{}>;
}
